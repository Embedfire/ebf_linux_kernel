# ebf_linux_kernel

#### 介绍
野火Linux产品线使用的内核

#### 说明
由于gitee仓库的大小限制，该仓库无法更新到最新提交内容，暂不用使用。



使用github地址：


```

/*6ULL*/

git clone -b ebf_4.19.35_imx6ul  https://github.com/Embedfire/ebf_linux_kernel.git

/*MP157*/

git clone -b ebf_4.19_star        https://github.com/Embedfire/ebf_linux_kernel.git

```



因为网络原因从github克隆困难的用户使用下面的gitee的地址：

```

/*6ULL*/

git clone  https://gitee.com/Embedfire/ebf_linux_kernel_6ull_depth1

/*MP157*/

git clone  https://gitee.com/Embedfire/ebf_linux_kernel_mp157_depth1

```

depth1名字gitee仓库master分支作为github仓库 ebf_4.19.35_imx6ul 或 ebf_4.19_star 分支的只保存了最新提交内容（git clone 使用 --depth=1 参数），可以直接使用，
如果需要查看历史提交差异内容可以从github网页里面访问。


